# RADAR Monitoring
## Description and Rationale
This repository will deploy Prometheus, Prometheus Operator, Grafana, and AlertManager using the kube-prometheus library. Jsonnet is used to create the resources from that library which are then converted to YAML to be applied to the cluster. Jsonnet allows for much more powerful 'templating' than is available with Helm for example. Also included is a dashboard for Grafana to utilize the metrics that Prometheus scrapes from OpenEthereum.

## CI/CD
The repository itself is rather barebones as the library itself is checked out and utilized as part of the CI pipeline. Deployment is also quite streamlined as having the Kubernetes cluster attached to the repository allows GitLab to handle credential configuration automatically.
